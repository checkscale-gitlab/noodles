#!/bin/sh
eval $(cat images.config)
cd initialize
cd alpine
podman build -t noodles/alpine:${NOODLES_IMAGES_VERSION} .
cd ..
cd javascript
podman build -t noodles/javascript:${NOODLES_IMAGES_VERSION}.
cd ..
cd kotlin
podman build -t noodles/kotlin:${NOODLES_IMAGES_VERSION} .
cd ..
cd golo
podman build -t noodles/golo:${NOODLES_IMAGES_VERSION} .
cd ..
cd python
podman build -t noodles/python:${NOODLES_IMAGES_VERSION} .
cd ..
cd scala
podman build -t noodles/scala:${NOODLES_IMAGES_VERSION} .
cd ..
cd go
podman build -t noodles/go:${NOODLES_IMAGES_VERSION} .
cd ..
cd java
podman build -t noodles/java:${NOODLES_IMAGES_VERSION} .
cd ..
cd ruby
podman build -t noodles/ruby:${NOODLES_IMAGES_VERSION} .