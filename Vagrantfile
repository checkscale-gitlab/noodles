# -*- mode: ruby -*-
# # vi: set ft=ruby :

# == Noodles's VM ==
NOODLES_DOMAIN = "noodles.test"
NOODLES_IP = "172.16.245.212"
NOODLES_PUB_IP = "192.168.1.212"
NOODLES_NAME = "noodles"

NOODLES_IMAGES_VERSION="1.0.0"

# 👋 don't forget to add IPs and domain in your hosts file

BOX_IMAGE = "bento/ubuntu-18.04"
VERSION = "1.0"

Vagrant.configure("2") do |config|
  config.vm.box = BOX_IMAGE

  ENV['LC_ALL']="en_US.UTF-8"

  config.vm.define "#{NOODLES_NAME}" do |node| 
    
    #node.vm.synced_folder ".", "/home/vagrant", id:"home"
    node.vm.synced_folder "./config", "/home/vagrant/config", id:"config"
    node.vm.synced_folder "./demos", "/home/vagrant/demos", id:"demos"
    node.vm.synced_folder "./docker", "/home/vagrant/docker", id:"docker"
    node.vm.synced_folder "./web", "/home/vagrant/web", id:"web"

    node.vm.hostname = "#{NOODLES_NAME}"
    node.vm.network "public_network", ip:"#{NOODLES_PUB_IP}",  bridge: "en0: Wi-Fi (AirPort)"
    # ⚠️ if you use Linux:
    # node.vm.network "public_network", ip:"#{NOODLES_PUB_IP}",  bridge: "wlp58s0"
    node.vm.network "private_network", ip: "#{NOODLES_IP}"

    node.vm.provider "virtualbox" do |vb|
      vb.memory = 1024
      vb.cpus = 1
      vb.name = "#{NOODLES_NAME}-vm"
    end

    node.vm.provision :shell, inline: <<-SHELL
      echo "⏳ setup..."

      # Add entries to hosts file:
      echo "" >> /etc/hosts
      echo '#{NOODLES_IP} #{NOODLES_DOMAIN}' >> /etc/hosts 
      echo "" >> /etc/hosts

    SHELL

    # vagrant provision --provision-with podman
    node.vm.provision "podman", privileged: true, type: "shell" , inline: <<-SHELL
      echo "--- podman ---"
      apt update
      apt -y  install software-properties-common
      add-apt-repository -y ppa:projectatomic/ppa
      apt -y install podman
      podman  info
    SHELL

    # vagrant provision --provision-with tools
    node.vm.provision "tools", privileged: true, type: "shell" , inline: <<-SHELL
      apt -y install jq
    SHELL

    # vagrant provision --provision-with nodejs
    node.vm.provision "nodejs", privileged: true, type: "shell" , inline: <<-SHELL
      # Install NodeJS
      curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
      apt-get -y install nodejs
    SHELL

    # vagrant provision --provision-with yarn
    node.vm.provision "yarn", privileged: true, type: "shell" , inline: <<-SHELL
      # Install yarn
      curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
      echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
      apt update
      apt install yarn

    SHELL

    # vagrant provision --provision-with registries
    node.vm.provision "registries", privileged: true, type: "shell" , inline: <<-SHELL
      echo "--- registries ---"
      mkdir -p /etc/containers
      curl https://raw.githubusercontent.com/projectatomic/registries/master/registries.fedora -o /etc/containers/registries.conf
      curl https://raw.githubusercontent.com/containers/skopeo/master/default-policy.json -o /etc/containers/policy.json
      
      cat /etc/containers/registries.conf
    SHELL

    # vagrant provision --provision-with initialize-ruby
    node.vm.provision "initialize-ruby", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize ruby ---"
      cd $HOME/config/initialize/ruby; podman build -t noodles/ruby:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-python
    node.vm.provision "initialize-python", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize python ---"
      cd $HOME/config/initialize/python; podman build -t noodles/python:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-node
    node.vm.provision "initialize-node", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize node ---"
      cd $HOME/config/initialize/node; podman build -t noodles/node:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-golang
    node.vm.provision "initialize-golang", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize golang ---"
      cd $HOME/config/initialize/golang; podman build -t noodles/golang:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-golo
    node.vm.provision "initialize-golo", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize golo ---"
      cd $HOME/config/initialize/golo; podman build -t noodles/golo:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-java
    node.vm.provision "initialize-java", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize java ---"
      cd $HOME/config/initialize/java; podman build -t noodles/java:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-kotlin
    node.vm.provision "initialize-kotlin", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize kotlin ---"
      cd $HOME/config/initialize/kotlin; podman build -t noodles/kotlin:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-scala
    node.vm.provision "initialize-scala", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize scala ---"
      cd $HOME/config/initialize/scala; podman build -t noodles/scala:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with initialize-alpine
    node.vm.provision "initialize-alpine", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- initialize alpine ---"
      cd $HOME/config/initialize/alpine; podman build -t noodles/alpine:#{NOODLES_IMAGES_VERSION} .
      podman images --filter=reference='noodles/*'
    SHELL

    # vagrant provision --provision-with webapp
    node.vm.provision "webapp", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- webapp ---"
      cd $HOME/web
      yarn
    SHELL

    # = create sample functions

    # = Deploy Python sample
    PYTHON_DOCKERFILE_DEMO=<<~HEREDOC
    FROM noodles/python:#{NOODLES_IMAGES_VERSION}
    COPY main.py ./
    HEREDOC

    PYTHON_SRC_DEMO=<<~HEREDOC
    # Python demo
    print('👋 Hello, world! 🌍')
    HEREDOC

    PYTHON_CONFIG_DEMO=<<~HEREDOC
    description: "this is a python function"
    content-type: "text"
    command: "python ./main.py"
    HEREDOC


    # vagrant provision --provision-with create-hello-python
    node.vm.provision "create-hello-python", privileged: false, type: "shell" , inline: <<-SHELL
      echo "--- create-hello-python ---"
      FUNCTION_NAME="hello-python"
      REF_NAME="master"
      IMAGE_NAME="noodles-${FUNCTION_NAME}-${REF_NAME}-img"
      CONTAINER_NAME="noodles-${FUNCTION_NAME}-${REF_NAME}-container"
      # == generate source code
      mkdir -p $HOME/web/functions/${FUNCTION_NAME}-${REF_NAME}
      echo "#{PYTHON_DOCKERFILE_DEMO}" > $HOME/web/functions/${FUNCTION_NAME}-${REF_NAME}/Dockerfile
      echo "#{PYTHON_SRC_DEMO}" > $HOME/web/functions/${FUNCTION_NAME}-${REF_NAME}/main.py
      echo "#{PYTHON_CONFIG_DEMO}" > $HOME/web/functions/${FUNCTION_NAME}-${REF_NAME}/config.yml
      # == build image and start the function container
      cd $HOME/web/functions/${FUNCTION_NAME}-${REF_NAME}/
      # === build
      podman build -t ${IMAGE_NAME} .
      # === create and start the container
      # > warning 🖐 it works only with alpine flavored images
      # stop and remove the container (if exists)
      if [ "$(podman ps -q -f name=${CONTAINER_NAME})" ]; then
        podman stop ${CONTAINER_NAME}
        podman rm ${CONTAINER_NAME}
      fi      
      # start the container
      podman run -it -d --name ${CONTAINER_NAME} ${IMAGE_NAME}:latest sh
      # test it
      podman exec ${CONTAINER_NAME} python ./main.py
    SHELL

    # vagrant provision --provision-with start
    node.vm.provision "start", privileged: false, type: "shell", run: "always", inline: <<-SHELL
      echo "🚀 starting 🍜 server..."
      echo "⏳ 🐳 restart stopped 🍜 containers"
      podman restart --all
      echo "🍜 template 🐳 images:"
      podman images --filter=reference='noodles/*'
      echo "🍜 function 🐳 images:"
      podman images --filter=reference='noodles-*'
      echo "🍜 🐳 (running) containers:"
      podman ps -a --filter name="noodles-"
      #echo "🚀 🍜 Noodles ignition"
      #cd $HOME/web
      #npm start
      echo "to start the 🍜 Noodles platform:"
      echo "vagrant ssh"
      echo "cd web; npm start"
    SHELL

  end
  # end of VM

end